Plan of speech
==============

_Introduction_
  * History
    * Alonzo Church and Lambda Calculus
    * John McCarty and AI
    * Language appearance
    * AI winter
    * Moore's Outlaw
    * Raise of Lisp machines
  * Pros and cons
    * Benefits for developers
      * Parallelism and multithreading
    * Benefits for business owners (productivity, speed)
      * List of companies that uses and supports FP
    * Success stories
      * Lisp
        * Space odissey
        * AI
        * iRumba
      * Erlang

_Parallel Universe_
  * Main conceptions
  * Languages
    * Families (Lisp and ML)
    * Main languages (goal, destiny and syntax)
      * Lisps (CL, Scheme, Clojure)
        * Distinctive features
        * Multi-paradigms
        * Code example
        * All it gives
        * Books
      * Haskell
        * Purity
        * Ideas and community
        * Books
      * Erlang
        * Distinctive features
        * Ideas
        * Books
  * Functional programming in non-functional languages
    * JavaScript
    * Ruby
    * C++11     [just saying]
    * C# (LINQ) [just saying]

_In search of messiah_

_Conclusions and questions_


Materials
=========

* Paul Graham "Beating the averages"
* John Hughes "Why Functional Programming Matters"

Links
=====

* [Лямбда-исчисление](http://ru.wikipedia.org/wiki/%D0%9B%D1%8F%D0%BC%D0%B1%D0%B4%D0%B0-%D0%B8%D1%81%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5)
* [Лисп-машина](http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%81%D0%BF-%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D0%B0)
* [Haskell.org > Functional programming](http://www.haskell.org/haskellwiki/Functional_programming)
* [What are the benefits of functional programming?](http://stackoverflow.com/questions/128057/what-are-the-benefits-of-functional-programming)
* [What is functional, declarative and imperative programming?](http://stackoverflow.com/questions/602444/what-is-functional-declarative-and-imperative-programming/8357604)
* [Moore's Law](http://en.wikipedia.org/wiki/Moore%27s_law)
* [AI winter](http://en.wikipedia.org/wiki/AI_winter)
